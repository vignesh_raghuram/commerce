
import { Component, OnInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Http, Response, RequestOptions, Headers} from '@angular/http';
import { HttpModule} from '@angular/http';
import {NgModule} from '@angular/core';
//import {NotificationsService} from 'angular4-notify';
import { ActivatedRoute,Router} from "@angular/router";
//import { Routes,RouterModule} from "@angular/router";

@Component({
templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
loginData;
userlist: any
loginCre = {userEmail:"chandran@gmail.com",userPassword:"demo"}
  constructor(
    public route:ActivatedRoute,
    private http: Http,
    //protected notificationsService: NotificationsService,
    private router:Router
    ) {}
    login (){
      this.loginCre.userEmail = this.loginData.emailId
      this.loginCre.userPassword=this.loginData.password
      this.http.post('/authenticate',this.loginCre).subscribe(res1 => {
        console.log(res1)
        localStorage.setItem('loginSessToken', res1.json().token);
        localStorage.setItem('mailId', this.loginCre.userEmail); 
        this.http.get('/users'+"?userEmail="+this.loginData.emailId +"&userPassword="+ this.loginCre.userPassword).subscribe(res => {
        //  this.notificationsService.addInfo('Login success');
      this.router.navigate(["/index/dashboard"])
        });
      },err => {
       // this.notificationsService.addInfo('Login error');
        console.log('Something went wrong!');
      });
    }
    

  ngOnInit() {
    this.loginData={
      emailId:"chandran@gmail.com",
      password:"demo",
      rememberMe:""
    }
  }

}
