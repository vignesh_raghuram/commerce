import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router } from '@angular/router';
import { Http, Response, RequestOptions, Headers} from '@angular/http';
import { HttpModule} from '@angular/http';

@Component({
    selector: 'hub-list',
    templateUrl: 'hublist.component.html',
})
export class HublistComponent implements OnInit {
   public listData : any;
  constructor(public route:ActivatedRoute, private router:Router) {
   this.createInit();
}
createNew(){
   this.router.navigate(["/index/hub/create"])
}

   createInit(){
 this.listData= [{
    hubName:"velachery",
	hubAddress:"velachery",
	hubPincode:"600042",
    hubLandlineNo:"044456789",
    hubAdmin:"Thiru", 
    hubAdminNumber:"8124888145" 
},{
	hubName:"chetpet",
	hubAddress:"chetpet",
	hubPincode:"600031",
    hubLandlineNo:"044456710",
    hubAdmin:"Chandran", 
    hubAdminNumber:"8124888145"
	
},{
	hubName:"Thiruvallur",
	hubAddress:"Thiruvallur",
	hubPincode:"601201",
    hubLandlineNo:"044456711",
    hubAdmin:"Manoj", 
    hubAdminNumber:"8124888136"
	
}]
   }

  ngOnInit() {
  }

}
