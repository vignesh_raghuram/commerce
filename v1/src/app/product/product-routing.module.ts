import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProductlistComponent} from './productlist.component';
import{CreatproductComponent} from './createproduct.component';

const routes: Routes = [
    {
    path: '',
    component: ProductlistComponent,
    data: {
      title: 'Product'
    }
  },{
    path: 'create',
    component: CreatproductComponent,
    data: {
      title: 'Product'
    }
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ProductRoutingModule {}
