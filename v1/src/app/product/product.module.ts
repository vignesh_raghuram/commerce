import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// Components Routing
import { ProductlistComponent } from './productlist.component';
import { ProductRoutingModule } from './product-routing.module';
import{CreatproductComponent} from './createproduct.component';

@NgModule({
  imports: [
    CommonModule,
    ProductRoutingModule,
    FormsModule
  ],
  declarations: [
    ProductlistComponent ,
    CreatproductComponent
  ]
})
export class ProductModule { }
