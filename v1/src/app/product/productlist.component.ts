import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router } from '@angular/router';
import { Http, Response, RequestOptions, Headers} from '@angular/http';
import { HttpModule} from '@angular/http';

@Component({
    selector: 'hub-list',
    templateUrl: 'productlist.component.html',
})
export class ProductlistComponent implements OnInit {
   public listData : any;
  constructor(public route:ActivatedRoute, private router:Router) {

}
createNew(){
  this.router.navigate(["/index/product/create"])
}
  ngOnInit() {
  }

}
